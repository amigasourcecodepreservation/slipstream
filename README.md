Slipstream is a scrolling arcade game in the style of space harrier. Published by Microdeal in 1988 and created by Ian Potts for the Amiga.

## Development

SlipStream was written in about 3 months during the autumn of 1988. Written in 68k assembler.

## Open Source

Like most other games from that period this game has been available at various sites for many years, but in 2018 it was clearly given permission by Ian Potts to be released under an open source license, the MIT License. This is an ongoing effort to release the old Microdeal/Michtron-titles for legal preservation.

Sadly, any source code for the game might have been lost in time, so all we can offer at the moment is the image files for usage in an emulator. However, Ian is looking through his archives.

## Help!!

- Are you a former Microdeal/Michtron contact or worker and happen have an old backup of the source code for any of the platforms? Please  help us and send it to our email. It is totally fine to send it anonymously.
- Do you want to resource the game to restore the source code? Go ahead, but don’t forget to notify us so we can preserve it.

## Many thanks to 

John Symes for being positive about the project and giving permission, Ian Potts for being supportive of releasing the game and for creating this cool game.

## References

[Hall of light (Amiga)](http://hol.abime.net/1982)

[SlipStream Page](https://amigasourcepres.gitlab.io/page/s/slipstream/)
